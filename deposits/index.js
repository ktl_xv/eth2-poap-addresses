import { ethers } from "ethers";
import fs from "fs";

const abi = JSON.parse(fs.readFileSync("./deposit_contract_abi.json", "utf-8"));

const provider = new ethers.providers.JsonRpcProvider("http://localhost:8545");
// We'll do this secuentially, I don't want to kill my server. And easier error handling

const DEPOSIT_CONTRACT_CREATION_BLOCK = 11052984;

const LIMIT_BLOCK = 11320898; // Last genesis -7 days block Nov-24-2020 11:59:58 AM +UTC

const DEPOSIT_CONTRACT_ADDRESS = "0x00000000219ab540356cBB839Cbe05303d7705Fa";

const BLOCKS_PER_CHUNK = 2 ** 14;

const depositContract = new ethers.Contract(
  DEPOSIT_CONTRACT_ADDRESS,
  abi,
  provider
);

let maxScannedBlockNumber = DEPOSIT_CONTRACT_CREATION_BLOCK;

fs.readdirSync("./deposits").forEach((file) => {
  const [blockNumber] = file.split("-");

  // It looks like they are in order, but lets be sure
  if (maxScannedBlockNumber < parseInt(blockNumber)) {
    maxScannedBlockNumber = parseInt(blockNumber);
  }
});

console.log(`Last proccesed block: ${maxScannedBlockNumber}`);

const filter = depositContract.filters.DepositEvent();

const pendingBlocks = LIMIT_BLOCK - maxScannedBlockNumber;

console.log(`Pending blocks: ${pendingBlocks}`);

const chunks = Math.ceil(pendingBlocks / BLOCKS_PER_CHUNK);

console.log(`Pending chunks: ${chunks}`);

for (let chunk = 0; chunk < chunks; chunk++) {
  const chunkStart = maxScannedBlockNumber + chunk * BLOCKS_PER_CHUNK;
  let chunkEnd = maxScannedBlockNumber + (chunk + 1) * BLOCKS_PER_CHUNK - 1;

  if (chunkEnd >= LIMIT_BLOCK) {
    console.log("Got to the limit block"); // To be sure we scanned everything
    chunkEnd = LIMIT_BLOCK;
  }

  console.log(`Parsing blocks ${chunkStart} to ${chunkEnd}`);
  const events = await depositContract.queryFilter(
    filter,
    chunkStart,
    chunkEnd
  );
  console.log(`Found ${events.length} deposits`);
  for await (const ev of events) {
    await depositHandler(ev);
  }
}

async function depositHandler(depositEvent) {
  const shortEvent = {
    blockNumber: depositEvent.blockNumber,
    transactionHash: depositEvent.transactionHash,
    pubkey: depositEvent.args.pubkey,
  };

  const transactionDetails = await provider.getTransactionReceipt(
    shortEvent.transactionHash
  );

  shortEvent.from = transactionDetails.from;
  const data = JSON.stringify(shortEvent);
  const shortPubkey = shortEvent.pubkey.substring(2, 10);
  fs.writeFileSync(
    `deposits/${shortEvent.blockNumber}-${shortEvent.transactionHash}-${shortPubkey}.json`,
    data
  );
}
